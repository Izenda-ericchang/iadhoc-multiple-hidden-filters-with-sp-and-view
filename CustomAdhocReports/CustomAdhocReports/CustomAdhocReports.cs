﻿using Izenda.BI.Framework.CustomConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using Izenda.BI.Framework.Models;
using System.Composition;
using Izenda.BI.Framework.Models.ReportDesigner;
using Izenda.BI.Framework.Utility;
using Izenda.BI.Framework.Models.Contexts; //For Referencing User Context

namespace CustomAdhocReports
{
    [Export(typeof(IAdHocExtension))]
    class CustomAdhocReports : DefaultAdHocExtension
    {
        #region  NEXT GEAR - SP HIDDEN FILTER
        //public override ReportFilterSetting SetHiddenFilters(SetHiddenFilterParam param)
        //{
        //    var regionFilter = "@regionName";

        //    int addHiddenFilters(ReportFilterSetting result, int filterPosition, QuerySource querySource, QuerySourceField field, Guid equalOperator, Relationship rel)
        //    {
        //        var firstFilter = new ReportFilterField
        //        {
        //            Alias = $"regionName{filterPosition}",
        //            QuerySourceId = querySource.Id,
        //            SourceDataObjectName = querySource.Name,
        //            QuerySourceType = querySource.Type,
        //            QuerySourceFieldId = field.Id,
        //            SourceFieldName = field.Name,
        //            DataType = field.DataType,
        //            Position = ++filterPosition,
        //            OperatorId = equalOperator,
        //            Value = "North America",
        //            RelationshipId = rel?.Id,
        //            IsParameter = true,
        //            ReportFieldAlias = null
        //        };

        //        result.FilterFields.Add(firstFilter);

        //        return filterPosition;
        //    }

        //    var filterSetting = new ReportFilterSetting()
        //    {
        //        FilterFields = new List<ReportFilterField>()
        //    };
        //    var position = 0;

        //    var ds = param.ReportDefinition.ReportDataSource;

        //    // Build the hidden filters for ship country fields
        //    foreach (var querySource in param.QuerySources // Scan thru the query sources that are involved in the report
        //        .Where(x => x.QuerySourceFields.Any(y => y.Name.Equals(regionFilter, StringComparison.OrdinalIgnoreCase)))) // Take only query sources that have filter field name
        //    {
        //        // Pick the relationships that joins the query source as primary source
        //        // Setting the join ensure the proper table is assigned when using join alias in the UI
        //        var rels = param.ReportDefinition.ReportRelationship.
        //            Where(x => x.JoinQuerySourceId == querySource.Id)
        //            .ToList();

        //        // Count the relationships that the filter query source is foreign query source
        //        var foreignRelCounts = param.ReportDefinition.ReportRelationship
        //            .Where(x => x.ForeignQuerySourceId == querySource.Id)
        //            .Count();

        //        // Find actual filter field in query source
        //        var field = querySource.QuerySourceFields.FirstOrDefault(x => x.Name.Equals(regionFilter, StringComparison.OrdinalIgnoreCase));

        //        // Pick the equal operator
        //        var equalOperator = Izenda.BI.Framework.Enums.FilterOperator.FilterOperator.EqualsManualEntry.GetUid();

        //        // In case there is no relationship that the query source is joined as primary
        //        if (rels.Count() == 0)
        //        {
        //            // Just add hidden filter with null relationship
        //            position = addHiddenFilters(filterSetting, position, querySource, field, equalOperator, null);
        //        }
        //        else
        //        {
        //            if (foreignRelCounts > 0)
        //            {
        //                position = addHiddenFilters(filterSetting, position, querySource, field, equalOperator, null);
        //            }
        //            foreach (var rel in rels)
        //            {
        //                // Loop thru all relationships that the query source is joined as primary and add the hidden field associated with each relationship
        //                position = addHiddenFilters(filterSetting, position, querySource, field, equalOperator, rel);
        //            }
        //        }
        //    }

        //    return filterSetting;
        //}
        #endregion

        #region NEXT GEAR - VIEW HIDDEN FILTER
        //public override ReportFilterSetting SetHiddenFilters(SetHiddenFilterParam param)
        //{
        //    var regionFilter = "ShipCountry";

        //    int addHiddenFilters(ReportFilterSetting result, int filterPosition, QuerySource querySource, QuerySourceField field, Guid equalOperator, Relationship rel)
        //    {
        //        var firstFilter = new ReportFilterField
        //        {
        //            Alias = $"regionName{filterPosition}",
        //            QuerySourceId = querySource.Id,
        //            SourceDataObjectName = querySource.Name,
        //            QuerySourceType = querySource.Type,
        //            QuerySourceFieldId = field.Id,
        //            SourceFieldName = field.Name,
        //            DataType = field.DataType,
        //            Position = ++filterPosition,
        //            OperatorId = equalOperator,
        //            Value = "Sweden;#Finland",
        //            RelationshipId = rel?.Id,
        //            IsParameter = false,
        //            ReportFieldAlias = null
        //        };

        //        result.FilterFields.Add(firstFilter);

        //        return filterPosition;
        //    }

        //    var filterSetting = new ReportFilterSetting()
        //    {
        //        FilterFields = new List<ReportFilterField>()
        //    };
        //    var position = 0;

        //    var ds = param.ReportDefinition.ReportDataSource;

        //    // Build the hidden filters for ship country fields
        //    foreach (var querySource in param.QuerySources // Scan thru the query sources that are involved in the report
        //        .Where(x => x.QuerySourceFields.Any(y => y.Name.Equals(regionFilter, StringComparison.OrdinalIgnoreCase)))) // Take only query sources that have filter field name
        //    {
        //        // Pick the relationships that joins the query source as primary source
        //        // Setting the join ensure the proper table is assigned when using join alias in the UI
        //        var rels = param.ReportDefinition.ReportRelationship.
        //            Where(x => x.JoinQuerySourceId == querySource.Id)
        //            .ToList();

        //        // Count the relationships that the filter query source is foreign query source
        //        var foreignRelCounts = param.ReportDefinition.ReportRelationship
        //            .Where(x => x.ForeignQuerySourceId == querySource.Id)
        //            .Count();

        //        // Find actual filter field in query source
        //        var field = querySource.QuerySourceFields.FirstOrDefault(x => x.Name.Equals(regionFilter, StringComparison.OrdinalIgnoreCase));

        //        // Pick the equal operator
        //        var equalOperator = Izenda.BI.Framework.Enums.FilterOperator.FilterOperator.EqualsManualEntry.GetUid();

        //        // In case there is no relationship that the query source is joined as primary
        //        if (rels.Count() == 0)
        //        {
        //            // Just add hidden filter with null relationship
        //            position = addHiddenFilters(filterSetting, position, querySource, field, equalOperator, null);
        //        }
        //        else
        //        {
        //            if (foreignRelCounts > 0)
        //            {
        //                position = addHiddenFilters(filterSetting, position, querySource, field, equalOperator, null);
        //            }
        //            foreach (var rel in rels)
        //            {
        //                // Loop thru all relationships that the query source is joined as primary and add the hidden field associated with each relationship
        //                position = addHiddenFilters(filterSetting, position, querySource, field, equalOperator, rel);
        //            }
        //        }
        //    }

        //    return filterSetting;
        //}
        #endregion

        #region NEXT GEAR - COMBINED (SP & VIEW)
        public override ReportFilterSetting SetHiddenFilters(SetHiddenFilterParam param)
        {
            var filterSetting = new ReportFilterSetting() { FilterFields = new List<ReportFilterField>() };

            AddHiddenFilter(param, filterSetting, "ShipCountry", new List<string> { "USA", "Canada" }); // for view
            AddHiddenFilter(param, filterSetting, "@regionName", new List<string> { "North America" }, isSP: true); // for sp

            return filterSetting;
        }

        private ReportFilterSetting AddHiddenFilter(SetHiddenFilterParam param, ReportFilterSetting filterSetting, string filterFieldName, List<string> values, bool isSP = false)
        {
            void addHiddenFilters(QuerySource querySource, QuerySourceField field, Guid operatorId, Relationship rel)
            {
                var value = string.Join(";#", values);
                var filterPosition = filterSetting.FilterFields.Count + 1;

                var filter = new ReportFilterField
                {
                    Alias = $"{field.Name}{filterPosition}",
                    QuerySourceId = querySource.Id,
                    SourceDataObjectName = querySource.Name,
                    QuerySourceType = querySource.Type,
                    QuerySourceFieldId = field.Id,
                    SourceFieldName = field.Name,
                    DataType = field.DataType,
                    Position = filterPosition,
                    OperatorId = operatorId,
                    Value = value,
                    RelationshipId = rel?.Id,
                    IsParameter = isSP,
                    ReportFieldAlias = null
                };

                filterSetting.FilterFields.Add(filter);
            }

            // Scan thru the query sources/fields that are involved in the report
            foreach (var querySource in param.QuerySources.Where(x => x.QuerySourceFields.Any(y => y.Name.Equals(filterFieldName, StringComparison.OrdinalIgnoreCase))))
            {
                // Pick the relationships that joins the query source as primary source
                // Setting the join ensure the proper table is assigned when using join alias in the UI
                var rels = param.ReportDefinition.ReportRelationship.Where(x => x.JoinQuerySourceId == querySource.Id);

                // Count the relationships that the filter query source is foreign query source
                var foreignRelCounts = param.ReportDefinition.ReportRelationship.Where(x => x.ForeignQuerySourceId == querySource.Id).Count();

                // Find actual filter field in query source
                var field = querySource.QuerySourceFields.FirstOrDefault(x => x.Name.Equals(filterFieldName, StringComparison.OrdinalIgnoreCase));

                // Get the filter operator GUID
                var equalOperator = Izenda.BI.Framework.Enums.FilterOperator.FilterOperator.EqualsManualEntry.GetUid();

                // In case there is no relationship that the query source is joined as primary
                if (!rels.Any())
                {
                    // Just add hidden filter with null relationship
                    addHiddenFilters(querySource, field, equalOperator, null);
                }
                else
                {
                    if (foreignRelCounts > 0)
                    {
                        addHiddenFilters(querySource, field, equalOperator, null);
                    }

                    foreach (var rel in rels)
                    {
                        // Loop thru all relationships that the query source is joined as primary and add the hidden field associated with each relationship
                        addHiddenFilters(querySource, field, equalOperator, rel);
                    }
                }
            }

            return filterSetting;
        }
        #endregion
    }
}
